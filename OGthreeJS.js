var demoRunning = 0;
var axisSize = 1;
//  group for vertexhandles
var group;
var map;
var triangles;
var geometry;
var sceneObject;
// Materials
var materials = [

    new THREE.MeshLambertMaterial({
        color: 0xffffff,
        shading: THREE.FlatShading,
        vertexColors: THREE.VertexColors
    }),
    new THREE.MeshBasicMaterial({
        color: 0x000000,
        shading: THREE.FlatShading,
        wireframe: true,
        transparent: true
    })

];

// From interactive draggablecubes
var container;
var camera, controls, scene, projector, renderer;
var mouse = new THREE.Vector2(),
    offset = new THREE.Vector3(),
    INTERSECTED;
var selectedHandle;
var objects;
var plane;

renderer = new THREE.WebGLRenderer({
    antialias: true
});
renderer.setSize(window.innerWidth, window.innerHeight);

// initVertexDemo();



window.addEventListener('resize', onWindowResize, false);

animate();

/*GUI----------------------------*/
var gui, Height = 5 * 32 - 1;
var sldr;
var sldrOld;

window.onload = function() {
    // sldr = new V3Slider();
    sldr = new THREE.Vector3(0, 0, 0);
    sldrOld = new THREE.Vector3(0, 0, 0);

    // var gui = new dat.GUI({
    //     height: guiHeight
    // });

    var gui = new dat.GUI();

    gui.add(this, 'axisSize').min(3).max(100).step(1).listen();
    gui.add(sldr, 'x').min(0).max(10).step(1).listen();
    gui.add(sldr, 'y').min(0).max(10).step(1).listen();
    gui.add(sldr, 'z').min(0).max(10).step(1).listen();

};

function initVertexDemo() {
    demoRunning = 1;
    axisSize = 2;
    objects = [];
    selectedHandle = new OG.VertexHandle();
    group = [];
    initScene();
    renderer.domElement.addEventListener('mousemove', onDocumentMouseMove, false);
    renderer.domElement.addEventListener('mousedown', onDocumentMouseDown, false);
}

function initProceduralDemo() {
    renderer.domElement.removeEventListener('mousemove', onDocumentMouseMove, false);
    renderer.domElement.removeEventListener('mousedown', onDocumentMouseDown, false);
    demoRunning = 2;
    if (axisSize < 3) {
        axisSize = 3;
    }
    initScene();
}

function initScene() {

    var objectSize = new THREE.Vector3(axisSize, axisSize, axisSize);

    projector = new THREE.Projector();
    raycaster = new THREE.Raycaster();



    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 10000);
    controls = new THREE.TrackballControls(camera, container);
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    map = new Array();
    for (a = 0; a < axisSize; a++) {
        map[a] = new Array();
        for (b = 0; b < axisSize; b++) {
            map[a][b] = new Array();
        }
    }

    triangles = [];



    // Scene
    scene = new THREE.Scene();

    geometry = new THREE.Geometry();
    var x = 0;
    var y = 0;
    var z = 0;
    for (y = 0; y < objectSize.y; y++) {
        for (x = 0 + y; x < objectSize.x - y; x++) {

            for (z = 0 + y; z < objectSize.z - y; z++) {
                // if (axisSize == 1 || x == y  || y == z || z == x) {
                map[x][y][z] = true;

                // } else {
                //     map[x][y][z] = false;
                // }
            }
        }
    }

    // Blocks. Current room we are generating polygons to
    for (x = 0; x < objectSize.x; x++) {
        for (y = 0; y < objectSize.y; y++) {
            for (z = 0; z < objectSize.z; z++) {
                // Polygons. Make polygons for the block
                if (map[x][y][z]) {
                    var blockPosition = new THREE.Vector3(x, y, z);
                    var orientation = OG.Orientations.zPlus; // Default orientation

                    // Upper polygon (north)
                    if (objectSize.z - 1 < z + 1 || !map[x][y][z + 1]) { // Make a upper polygon if there is no a block
                        orientation = OG.Orientations.zPlus; // polygon goes from west to east
                        createPolygon(orientation, blockPosition);
                    }

                    // Right polygon (east)
                    if (objectSize.x - 1 < x + 1 || !map[x + 1][y][z]) {
                        orientation = OG.Orientations.xPlus; // north to south
                        createPolygon(orientation, blockPosition);
                    }
                    // Bottom polygon (south)
                    if (0 > z - 1 || !map[x][y][z - 1]) {
                        orientation = OG.Orientations.zMinus; // east to west;
                        createPolygon(orientation, blockPosition);
                    }
                    // Left polygon (west)
                    if (0 > x - 1 || !map[x - 1][y][z]) {
                        orientation = OG.Orientations.xMinus; // south to north
                        createPolygon(orientation, blockPosition);
                    }
                    // Roof
                    if (objectSize.y - 1 < y + 1 || !map[x][y + 1][z]) {
                        orientation = OG.Orientations.yPlus; // south to north
                        createPolygon(orientation, blockPosition);
                    }
                    // Floor
                    if (0 > y - 1 || !map[x, y - 1, z]) {
                        orientation = OG.Orientations.yMinus; // south to north;
                        createPolygon(orientation, blockPosition);
                    }
                }
            }
        }
    }

    geometry.computeFaceNormals();
    geometry.computeVertexNormals();



    // Lights
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(1, 2, 3);
    scene.add(light);

    light = new THREE.DirectionalLight(0xffffff);

    light.position.set(-1, -2, -3);
    scene.add(light);

    camera.position.z = axisSize * 2;

    // Colors for the mesh
    var faceIndices = ['a', 'b', 'c', 'd'];

    var color, f, p, n, vertexIndex, radius = 200;

    for (var i = 0; i < geometry.faces.length; i++) {

        f = geometry.faces[i];

        n = (f instanceof THREE.Face3) ? 3 : 4;

        for (var j = 0; j < n; j++) {

            vertexIndex = f[faceIndices[j]];

            p = geometry.vertices[vertexIndex];

            color = new THREE.Color(0xffffff);
            color.setHSL((p.y / radius + 1) / 2, 1.0, 0.5);

            f.vertexColors[j] = color;
        }
    }



    sceneObject = new THREE.Mesh(geometry, materials[0]);

    scene.add(sceneObject);
    objects.push(sceneObject);

    // console.log(geometry);
    // console.log(geometry.vertices);
    // console.log(triangles);


}



/*-----------------------------------*/

// Create single polygon for the block

function createPolygon(orientation, blockPosition) {
    // Vertices aka vertexes

    var leftBottom = createVertex(orientation, blockPosition, OG.Alignments.LeftBottom);
    var rightBottom = createVertex(orientation, blockPosition, OG.Alignments.RightBottom);
    var leftUpper = createVertex(orientation, blockPosition, OG.Alignments.LeftUpper);
    var rightUpper = createVertex(orientation, blockPosition, OG.Alignments.RightUpper);

    // Create bottom and upper triangles for the polygon
    createTriangles(leftBottom, rightBottom, leftUpper, rightUpper);
}

function createTriangles(bottomLeft, bottomRight, upperLeft, upperRight) {
    geometry.faces.push(new THREE.Face3(upperLeft, bottomLeft, upperRight));
    geometry.faces.push(new THREE.Face3(bottomLeft, bottomRight, upperRight));

    geometry.computeBoundingSphere();
}

// Determines the position of a vertex with block position and vertex's aligment on the block's polygon 

function createVertex(orientation, blockPosition, alignment) {
    var vertexPosition = new THREE.Vector3(blockPosition.x, blockPosition.y, blockPosition.z);

    // TexturePart tempTexturePart = new TexturePart (0);

    switch (orientation) {
        case 0: // Z+ , polygon is at z+ direction from the center of a block
            // Add polygon's position to vertexPosition
            vertexPosition.z += 1;

            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Zplus;

            //x
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.x++;
            }
            //y
            if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
                vertexPosition.y++;
            }

            break;
        case 1: // Z-
            // Add polygon's position to vertexPosition
            vertexPosition.x += 1;

            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Zminus;

            //X
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.x--;
            }
            //y
            if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
                vertexPosition.y++;
            }
            break;
        case 4: // X+
            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Xminus;

            //z
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.z++;
            }
            //y
            if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
                vertexPosition.y++;
            }

            break;
        case 5: // X-
            // Add polygon's position to vertexPosition
            vertexPosition.x += 1;
            vertexPosition.z += 1;

            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Xplus;

            //z
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.z--;
            }
            //y
            if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
                vertexPosition.y++;
            }
            break;
        case 2: // Roof
            vertexPosition.y += 1;

            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Yplus;

            //x
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.x++;
            }
            //z
            if (alignment == OG.Alignments.LeftBottom || alignment == OG.Alignments.RightBottom) {
                vertexPosition.z++;
            }
            break;
        case 3: // Floor
            // if (_blockTextureParts != null)
            //         tempTexturePart = _blockTextureParts [blockPosition].Yminus;

            //x
            if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
                vertexPosition.x++;
            }
            //z
            if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
                vertexPosition.z++;
            }
            break;
    }

    var newVertex = OG.vertex.vertex(geometry.vertices.length - 1, alignment);

    geometry.vertices.push(new THREE.Vector3(vertexPosition.x, vertexPosition.y, vertexPosition.z));

    if (demoRunning == 1) {
        createVertexHandle(vertexPosition);
    }

    return geometry.vertices.length - 1;
}


function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function animate() {
    if (demoRunning > 0) {

        // sceneObject.rotation.x += 0.01;
        // sceneObject.rotation.y += 0.01;


        if (demoRunning == 1 && selectedHandle.selectedElement && (sldr.x != sldrOld.x || sldr.y != sldrOld.y || sldr.z != sldrOld.z)) {
            // console.log(selectedHandle.vertices.length);
            var sldrV3 = new THREE.Vector3(sldr.x, sldr.y, sldr.z);
            selectedHandle.selectedElement.position = sldrV3;

            // for (i = 0; i < geometry.vertices.length; i++) {
            //     var distance = geometry.vertices[i].distanceTo(sldrV3);
            //     console.log("distance between slider value and vertex: " + distance);
            //     if (distance < 0.3) {
            //         geometry.vertices[i] = sldrV3;
            //     }
            // }

            // XML3D style
            for (i = 0; i < selectedHandle.vertices.length; i++) {
                var vertexIndex = selectedHandle.vertices[i];
                // console.log(vertexIndex);
                // console.log(selectedHandle.vertices[i]);

                // console.log(sceneObject.geometry.vertices[vertexIndex].x);

                sceneObject.geometry.vertices[vertexIndex] = new THREE.Vector3(selectedHandle.selectedElement.position.x, selectedHandle.selectedElement.position.y, selectedHandle.selectedElement.position.z);
                // console.log(geometry.vertices[i].position);

                // sceneObject = new THREE.Mesh(geometry, materials[0]);
                // sceneObject.position.x = selectedHandle.selectedElement.position.x;
                // scene.add(sceneObject);
                sceneObject.geometry.dynamic = true
                sceneObject.geometry.verticesNeedUpdate = true
                sceneObject.geometry.computeFaceNormals();
                sceneObject.geometry.computeVertexNormals();
            }

            sldrOld.x = sldr.x;
            sldrOld.y = sldr.y;
            sldrOld.z = sldr.z;
        }
        render();
    }

    requestAnimationFrame(animate);


}

function render() {

    controls.update();

    renderer.render(scene, camera);

}

function createVertexHandle(vertexPosition) {
    // console.log("createVertexHandles");
    // console.log("vtxPos");
    // console.log(vertexPosition );
    // console.log("----");
    found = false;
    // selectedHandle.vertices.push(geometry.vertices.length);
    for (i = 0; i < group.length; i++) {
        // console.log(gr oup[i] );

        if (group[i] && group[i].selectedElement.position.x == vertexPosition.x && group[i].selectedElement.position.y == vertexPosition.y && group[i].selectedElement.position.z == vertexPosition.z) {
            // console.log("vertex handle already in the position!");
            found = true;

            group[i].vertices.push(geometry.vertices.length - 1);
        }
    }
    if (found == false) {
        // console.log("created a new vertex handle");


        var size = 0.1;
        var handleGeometry = new THREE.CubeGeometry(size, size, size);

        handleMesh = new THREE.Mesh(handleGeometry, materials[0]);
        handleMesh.position = vertexPosition;

        var newHandle = new OG.VertexHandle();
        newHandle.selectedElement = handleMesh;
        newHandle.vertices.push(geometry.vertices.length - 1);
        group.push(newHandle);

        scene.add(handleMesh);
        // objects.push(handleMesh);
    }


    // var transformId = "vertexHandle" + vertexPosition.x  + vertexPosition.y + vertexPosition.z;
    // var groupId = "vertexHandleGroup" + vertexPosition.x + vertexPosition.y  + vertexPosition.z;

    // var group = document.getElementById(groupId);
    // if (group) {
    //   console.log("vertex handle already in the position! " + group);
    //   var vtxCount = group.getAttribute("vertexCount");
    //   console.log("vtxCount " + vtxCount);
    //   if (vtxCount == 1) {
    //     group.setAttribute("index2", verticesLength);
    //     group.setAttribute("vertexCount", 2); //TODO clean these

    //   } else if (vtxCount == 2) {
    //     group.setAttribute("index3", verticesLength);
    //     group.setAttribute("vertexCount", 3);

    //   } else if (vtxCount == 3) {
    //     group.setAttribute("index4", verticesLength);
    //     group.setAttribute("vertexCount", 4);

    //   }
    // } else {
    //   console.log("creating a new vertex handle")
    //   var xml3d = document.getElementById("xml3d");
    //   var defs = document.getElementById("defs");
    //   var mesh = document.getElementById("cube");

    //   for (x = 0; x < 1; x++) {

    //     var transform = document.createElementNS("http://www.xml3d.org/2009/xml3d", "transform");
    //     var group = document.createElementNS("http://www.xml3d.org/2009/xml3d", "group");
    //     var mesh = document.createElementNS("http://www.xml3d.org/2009/xml3d", "mesh");


    //     // var posX = 3 * x;
    //     // var posZ = 3;
    //     // var posY = 3;
    //     // var translation = posX + " " + posY + " " + posZ;
    //     var translation = vertexPosition.x + " " + vertexPosition.y + " " + vertexPosition.z;
    //     // console.log("translation");
    //     group.setAttribute("id", groupId);
    //     transform.setAttribute("id", transformId);
    //     transform.setAttribute("translation", translation);
    //     transform.setAttribute("scale", "0.1 0.1 0.1");
    //     group.setAttribute("vertexCount", 1);

    //     mesh.setAttribute("src", "#cube");
    //     group.setAttribute("shader", "#HandleShader");
    //     // ID reference string that points to transform
    //     group.setAttribute("transform", "#" + transformId); 
    //     group.setAttribute("onmouseover", "OG.mouseOver(this,'#HandleShader')");
    //     group.setAttribute("onmouseout", "OG.mouseOut(this,'      #HandleShader    ')");
    //     group.setAttribute("onclick", "OG.click(this     )");
    //     // How many different vertices in this position?

    //     group.setAttribute("index1", verticesLength);


    //     defs.appendChild(transform);
    //     group.appendChild(mesh);
    //     xml3d.appendChild(group);

    //   }
    // }
}

function onDocumentMouseDown(event) {

    // console.log("mouse down");

    event.preventDefault();

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    projector.unprojectVector(vector, camera);

    // var rayLength = 100;

    var destination = vector.sub(camera.position).normalize(); //.multiplyScalar(rayLength)
    // console.log("destination");
    // console.log(destination);

    var raycaster = new THREE.Raycaster(camera.position, destination);

    var intersects = raycaster.intersectObjects(objects);

    // console.log("point: ");
    // console.log(intersects[0].point);

    // Select closest handle
    if (intersects.length > 0) {
        var closestOne = 0;
        var closestDistance = group[0].selectedElement.position.distanceTo(intersects[0].point);
        for (i = 1; i < group.length; i++) {
            var distance = group[i].selectedElement.position.distanceTo(intersects[0].point);
            // console.log("distance: " + distance);
            if (distance < closestDistance) {
                closestOne = i;
                closestDistance = distance;
            }
            // console.log("closestOne: " + closestOne);
        }

        // console.log("found the handle");
        // Give default material back for the previous handle

        if (selectedHandle.selectedElement != null) {
            selectedHandle.selectedElement.material = materials[0];
        }
        selectedHandle = group[closestOne];
        selectedHandle.selectedElement.material = materials[1];

        // console.log("-----");

        // THREE style vertex selection
        closestOne = 0;
        closestDistance = geometry.vertices[0].distanceTo(intersects[0].point);
        for (i = 1; i < geometry.vertices.length; i++) {
            var distance = geometry.vertices[i].distanceTo(intersects[0].point);
            // console.log("distance: " + distance);
            if (distance < closestDistance) {
                closestOne = i;
                closestDistance = distance;
            }
            // console.log("closestOne: " + closestOne);
        }



        // // XML3D style vertex selection
        // for (i = 0; i < group.length; i++) {
        //     if (group[i] && group[i].selectedElement.position.x == intersects[0].object.position.x && group[i].selectedElement.position.y == intersects[0].object.position.y && group[i].selectedElement.position.z == intersects[0].object.position.z) {
        //         console.log("found the handle");
        //         // group[i].selectedElement = intersects[0].object;
        //         selectedHandle = group[i];

        // // Give default material back for the previous handle
        // if (selectedHandle.selectedElement != null) {
        //     selectedHandle.selectedElement.material = materials[0];
        // }
        // selectedHandle = group[closestOne];
        // selectedHandle.selectedElement.material = materials[1];
        //     }
        // }

        // console.log(selectedElement);

        // var intersects = raycaster.intersectObject(plane);
        // offset.copy(intersects[0].point).sub(plane.position);

        // container.style.cursor = 'move';

        // GUI
        // console.log("sldr.x");

        // console.log(sldr.x);



        // sldr.x = geometry.vertices[closestOne].x;
        // sldr.y = geometry.vertices[closestOne].y;
        // sldr.z = geometry.vertices[closestOne].z;

        sldr.x = selectedHandle.selectedElement.position.x;
        sldr.y = selectedHandle.selectedElement.position.y;
        sldr.z = selectedHandle.selectedElement.position.z;

    }

}

function onDocumentMouseMove(event) {

    event.preventDefault();

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    // console.log("selectedElement");
    // console.log(selectedElement);


}

// function onDocumentMouseUp( event ) {

//     event.preventDefault();

//     controls.enabled = true;

//     if ( INTERSECTED ) {

//         plane.position.copy( INTERSECTED.position );

//         SELECTED = null;

//     }

//     container.style.cursor = 'auto';

// }