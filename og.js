var OG = {};

OG.Orientations = {
	zPlus: 0,
	zMinus: 1,
	yPlus: 2,
	yMinus: 3,
	xMinus: 4,
	xPlus: 5
}

OG.Alignments = {
	LeftBottom: 0,
	RightBottom: 1,
	LeftUpper: 2,
	RightUpper: 3
}

// Texture parts of one object block
OG.vertex = {
	'_index': 0,
	'_alignment': OG.Alignments.LeftBottom,
	'vertex': function(index, alignment) {
		this._index = index;
		this._alignment = alignment;
	}
};

//TODO
OG.VertexHandle = function() {
	this.selectedElement;
	// Vertices at this location
	this.vertices = [];
	// this.group = [];
}